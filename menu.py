import telnetlib
import os
import logging


controlport = 30009
logging.basicConfig(filename='/var/log/terminal.log', format='%(asctime)s - %(message)s', level=logging.INFO)

def clearbynumber(port):
    try:
        os.system("clear")
        tn2 = telnetlib.Telnet("127.0.0.1", port, timeout=1)
        ans = tn2.read_until(b">", timeout=1)
        ans = str(ans)
        tn2.close()
        if "in use" in ans:
            tn3 = telnetlib.Telnet("127.0.0.1", controlport, timeout=1)
            command = "disconnect " + str(port) + "\r\n"
            exit = "exit\r\n"
            tn3.write(str.encode(command))
            tn3.write(str.encode(exit))
            tn3.close()
            print("Clear session port " + str(port) + " successful")
        else:
            print("Port " + str(port) + " already available")
    except:
        print("Port " + str(port) + " no physical connection")
        tn2.close()
    print("\n\n\n\n")
    out = input("Press Enter to back to menu")
    return


run = 1
while run == 1:
    os.system('clear')
    print(""" 
********************************************************
 Terminal Server: Raspberry Terminal
 To exit form a device, use CTRL+] then enter "quit"
********************************************************

	1			Connect to Device 1
	2			Connect to Device 2
	3			Connect to Device 3
	4			Connect to Device 4
	5			Connect to Device 5
	6			Connect to Device 6
	7			Connect to Device 7	
	8			Connect to Device 8
	s			Show all established sessions
	c			Clear sessions menu
	c[number]	        Clear session by number eg. c1 c2 c8
	e			Menu-exit
	q			Quit Terminal server session

""")

    answer = input("Your selection :  ")

    if answer == "1":
        os.system("clear")
        logging.info("telnet localhost 30001")
        os.system("telnet localhost 30001")
        logging.info("disconnect 30001")
    elif answer == "2":
        os.system("clear")
        logging.info("telnet localhost 30002")
        os.system("telnet localhost 30002")
        logging.info("disconnect 30002")
    elif answer == "3":
        os.system("clear")
        logging.info("telnet localhost 30003")
        os.system("telnet localhost 30003")
        logging.info("disconnect 30003")
    elif answer == "4":
        os.system("clear")
        logging.info("telnet localhost 30004")
        os.system("telnet localhost 30004")
        logging.info("disconnect 30004")
    elif answer == "5":
        os.system("clear")
        logging.info("telnet localhost 30005")
        os.system("telnet localhost 30005")
        logging.info("disconnect 30005")
    elif answer == "6":
        os.system("clear")
        logging.info("telnet localhost 30006")
        os.system("telnet localhost 30006")
        logging.info("disconnect 30006")
    elif answer == "7":
        os.system("clear")
        logging.info("telnet localhost 30007")
        os.system("telnet localhost 30007")
        logging.info("disconnect 30007")
    elif answer == "8":
        os.system("clear")
        logging.info("telnet localhost 30008")
        os.system("telnet localhost 30008")
        logging.info("disconnect 30008")
    elif answer == "e":
        logging.info("exit")
        run = 0
    elif answer == "q":
        logging.info("quit")
        run = 0
    elif answer == "s":
        os.system("clear")
        logging.info("Show all established sessions")
        port = 30001
        while port <= 30008:
            try:
                portshow = port
                port = port + 1
                tn = telnetlib.Telnet("127.0.0.1", portshow, timeout=1)
                ans = tn.read_until(b">", timeout=1)
                ans = str(ans)
                if "in use" in ans:
                    print("Port " + str(portshow) + " In use")
                else:
                    print("Port " + str(portshow) + " Available")
                tn.close()
            except:
                print("Port " + str(portshow) + " no physical connection")
                tn.close()
        print("\n\n\n\n")

        out = input("Press Enter to back to main menu")

    elif answer == "c":
        os.system("clear")
        while True:
            check = [0, 0, 0, 0, 0, 0, 0, 0]
            cstat = 0
            port = 30001
            while port <= 30008:
                try:
                    portshow = port
                    port = port + 1
                    tn = telnetlib.Telnet("127.0.0.1", portshow, timeout=1)
                    ans = tn.read_until(b">", timeout=1)
                    ans = str(ans)
                    if "in use" in ans:
                        print("Port " + str(portshow) + " In use")
                        check[portshow - 30001] = 1
                        cstat = 1

                    tn.close()
                except:
                    check[portshow - 30001] = 2
                    tn.close()
            if cstat == 0:
                print(" No Connection in use")
                print("\n\n\n\n")
                out = input("Press Enter to back to main menu")
                break

            print("\n\n\n\n")
            selectnum = input("select session number [ 1-8 ] or type [quit] to back to menu : ")
            if selectnum == "quit" or selectnum == "q":
                break
            if selectnum.isnumeric():
                try:
                    port = 30000 + int(selectnum)
                    if check[int(selectnum) - 1] == 0:
                        os.system("clear")
                        print("Port " + str(port) + " already available")
                        print("\n\n\n\n")
                        out = input("Press Enter to back to clear sessions menu")
                        os.system("clear")
                        continue

                    if check[int(selectnum) - 1] == 2:
                        os.system("clear")
                        print("Port " + str(port) + " no physical connection")
                        print("\n\n\n\n")
                        out = input("Press Enter to back to clear sessions menu")
                        os.system("clear")
                        continue

                    tn2 = telnetlib.Telnet("127.0.0.1", controlport, timeout=1)
                    command = "disconnect " + str(port) + "\r\n"
                    exit = "exit\r\n"
                    tn2.write(str.encode(command))
                    tn2.write(str.encode(exit))
                    tn2.close()
                    os.system("clear")
                    logging.info(command)
                    print("Clear session port " + str(port) + " Successful")
                except:
                    os.system("clear")
                    print("Please type session number [ 1-8 ] or [quit]")
                    continue
            else:
                os.system("clear")
                print("Wrong Input")
                continue

            print("\n\n\n\n")
            out = input("Press Enter to back to main menu")
            break

    elif answer == "c1":
       clearbynumber(30001)

    elif answer == "c2":
        clearbynumber(30002)

    elif answer == "c3":
        clearbynumber(30003)

    elif answer == "c4":
        clearbynumber(30004)

    elif answer == "c5":
        clearbynumber(30005)

    elif answer == "c6":
        clearbynumber(30006)

    elif answer == "c7":
        clearbynumber(30007)

    elif answer == "c8":
        clearbynumber(30008)

    else:
        print("Wrong Input")

os.system("clear")
