from getpass import getpass
from passlib.hash import nthash
import csv
import os
import logging

logging.basicConfig(filename='/var/log/usermanage.log', format='%(asctime)s - %(message)s', level=logging.INFO)

run = 1
while run==1:
	os.system("clear")
	print(""" 
********************************
Freeradius users management

********************************

	n	new user
	i	new user from csv file
	d	delete user
	r	delete user from csv file 
	q	quit

""")
	
	answer = input("Your selection : ")

	if answer=="q":
		logging.info("Exit")
		break
	elif answer=="n":
		os.system("clear")
		username = input("new username : ")
		same=0
		with open("/etc/freeradius/3.0/users", "r+") as f:
			lines = f.readlines()
			f.seek(0)
			for line in lines:
				if "\"" + username + "\"" in line:
					same=1
		if same==1:
			print("username already exists\n\n")
			out = input("press enter to back to menu")
			continue

		password = getpass("password : ")
		password2 = getpass("confirm password : ")
		if password != password2:
			print("password and confirm password not match\n\n\n")
			out = input("Press enter to back to menu")
			continue
		f=open("/etc/freeradius/3.0/users", "a+")
		hash = nthash.hash(password)
		if not nthash.verify(password,hash):
			print("hash error")
			out = input("Press enter to back to menu")
			continue
		f.write("\"" + username  + "\" NT-Password := \"" + hash.upper() + "\"\n")
		f.close()
		print("New user inserted successfully\n\n\n")
		logging.info("Inserted user " + username + " successfully")
		out = input("press enter to back to menu")
	elif answer=="d":
		found=1
		os.system("clear")
		username = input("delete username: ")
		with open("/etc/freeradius/3.0/users", "r+") as f:
			lines = f.readlines()
			f.seek(0)
			for line in lines:
				if "\"" + username + "\"" not in line:
					f.write(line)
				else:
					found=0
			f.truncate()
		f.close()
		if found==1:
			print("username not found\n\n\n")
		else:
			logging.info("Deleted user : " + username + " successfully")
			print("delete sucessfully\n\n\n")
		out = input("Press enter to back to menu")
	elif answer=="i":
		os.system("clear")
		path = input("File path[Full path]:")
		try:
			with open(path) as csvfile:
				readCSV = csv.reader(csvfile, delimiter=',')
				for row in readCSV:
					same = 0
					username = row[0]
					password = row[1]
					with open("/etc/freeradius/3.0/users", "r+") as f:
						lines = f.readlines()
						f.seek(0)
						for line in lines:
							if "\"" + username + "\"" in line:
								same=1
					if same==1:
						print("username:" + username  + " already exists")
						continue

					f=open("/etc/freeradius/3.0/users", "a+")
					hash = nthash.hash(password)
					if not nthash.verify(password,hash):
						print("hash error")
						out = input("Press enter to back to menu")
					f.write("\"" + username  + "\" NT-Password := \"" + hash.upper() + "\"\n")
					f.close()
					print("User" + username + " inserted successfully")
					logging.info("Inserted user : " + username + " successfully")

		except:
			print("File Not Found")
		out = input("\n\n\npress enter to back to menu")

	elif answer=="r":
		os.system("clear")
		path = input("File path [Full path]:")
		try:
			with open(path) as csvfile:
				readCSV = csv.reader(csvfile,delimiter=',')
				for row in readCSV:
					username = row[0]
					with open("/etc/freeradius/3.0/users", "r+") as f:
						lines = f.readlines()
						f.seek(0)
						for line in lines:
							if "\"" + username + "\"" not in line:
								f.write(line)
							else:
								print("delete username:" + username + " sucessfully")
								logging.info("Deleted user : " + username + " successfully")
						f.truncate()
					f.close()
			out = input("\n\n\npress enter to back to menu")
		except:
			print("File not Found")
os.system("systemctl restart freeradius")
os.system("clear")
